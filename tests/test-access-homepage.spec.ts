import { test } from '@playwright/test';
import { HomePage } from '../pom/home.page';

test.describe('Inskru Homepage', () => {
  let homePage: HomePage;

  test.beforeEach(async ({ page }) => {
    homePage = new HomePage(page);

  });

  test('ไปหน้า homepage สำเร็จ', async ({ page }) => {
    await homePage.goto();
    await homePage.closePersuasiveModal();
    await homePage.shouldBeDisplayed();
  });

});
