import { Page, expect, test } from '@playwright/test';
import { HomePage } from '../pom/home.page';
import { LoginPage } from '../pom/login.page';
import { NewProfile } from '../pom/new-profile.page';
import { validUsers } from '../fixtures/users';

test.describe('insKru New Profile', () => {
    let homePage: HomePage;
    let loginPage: LoginPage;
    let newProfileUsers: NewProfile[] = [];

    test.beforeEach(async ({ page }) => {
        homePage = new HomePage(page);
        loginPage = new LoginPage(page);

        newProfileUsers = validUsers.map(user => new NewProfile(page, user));
    });

    validUsers.forEach((user, userIndex) => {
        test(`Using ${user.credential.login} accesses to profile tab`, async ({ page }) => {
            await homePage.goto();
            await homePage.closePersuasiveModal();
            await loginPage.goToLoginPage();
            await loginPage.loginWith(user.credential);
            await newProfileUsers[userIndex].goToProfileDashboard();
            await newProfileUsers[userIndex].shouldBeDisplayedAfterLogin();
            await page.getByRole('tab', { name: 'ไอเดีย' }).click();
            await page.getByRole('tab', { name: 'ไอเดียของฉัน'}).click();
            await expect (page.getByRole('tab', { name: 'ไอเดียของฉัน' })).toHaveText('ไอเดียของฉัน');
            await page.getByRole('tab', { name: 'ไอเดียที่ได้รับเครดิต'}).click();
            await expect (page.getByRole('tab', { name: 'ไอเดียที่ได้รับเครดิต' })).toHaveText('ไอเดียที่ได้รับเครดิต');
            await page.getByRole('tab', { name: 'แบบร่าง'}).click();
            await expect (page.getByRole('tab', { name: 'แบบร่าง' })).toHaveText('แบบร่าง');
            await page.getByRole('tab', { name: 'ไอเดียที่เก็บไว้'}).click();
            await expect (page.getByRole('tab', { name: 'ไอเดียที่เก็บไว้' })).toHaveText('ไอเดียที่เก็บไว้');
            await page.getByRole('tab', { name: 'เกียรติบัตร' }).click();
            await page.getByRole('link', { name: 'ผู้ติดตาม' }).click();
            await page.getByRole('tab', { name: 'กำลังติดตาม' }).click();
        });
    });
});
