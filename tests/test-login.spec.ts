import { test, expect } from '@playwright/test';

test('test', async ({ page }) => {
  await page.goto('https://newdev.inskru.com/');
  await page.getByLabel('Close', { exact: true }).click();
  await page.getByTestId('test-sign-in').click();
  await page.getByTestId('test-email').click();
  await page.getByTestId('test-email').fill('inskru.ad@gmail.com');
  await page.getByTestId('test-email').press('Tab');
  await page.getByTestId('test-password').fill('insAdmin01');
  await page.getByRole('img', { name: 'eye-close' }).click();
  await page.getByTestId('test-login-btn').click();
});