import { test, expect } from '@playwright/test';

const BASE_URL = 'https://newdev.inskru.com';

test('login สำเร็จ', async ({ page }) => {
  await page.goto(BASE_URL);
  await page.getByLabel('Close', { exact: true }).click();
  await page.getByTestId('test-sign-in').click();
  const currentURL = page.url();
//   await page.waitForNavigation();
  expect(currentURL).toBe(`${BASE_URL}/auth/sign-in/`);

  await page.getByTestId('test-email').click();
  await page.getByTestId('test-email').fill('inskru.ad@gmail.com');
  await page.getByTestId('test-email').press('Tab');
  await page.getByTestId('test-password').fill('insAdmin01');
  await page.getByRole('img', { name: 'eye-close' }).click();
  await page.getByTestId('test-login-btn').click();

});
