import { Locator, Page, expect } from "@playwright/test";
import { app } from "../fixtures/app";

export class HomePage {
    readonly _page: Page;
    readonly _pageUrl: string;
    readonly _persuasiveModal: Locator;
    readonly _inskruLogo: Locator;
    readonly _titlePage: Locator;

    constructor(page: Page) {
        this._page = page;
        this._pageUrl = `${app.baseUrl}/idea-library`;
        this._persuasiveModal = page.getByLabel('Close',{exact : true});
        this._inskruLogo = page.getByRole('link', {name: 'icon', exact: true});
        this._titlePage = page.getByText('ห้องสมุดไอเดีย').nth(1);
    }
    async goto() {
        await this._page.goto(app.baseUrl);
    }    

    async closePersuasiveModal(){
        await this._persuasiveModal.click();
    }

    async shouldBeDisplayed() {
        await this._inskruLogo.click();
        await expect(this._titlePage).toBeVisible();
    }

}