import { Locator, Page, expect } from '@playwright/test';
import { app } from '../fixtures/app';
import { Credential } from '../interfaces/credential';
import { validUsers } from '../fixtures/users';

export class LoginPage {
    readonly _page: Page;
    readonly _pageUrl: string;
    readonly _loginField: Locator;
    readonly _passwordField: Locator;
    readonly _loginButton: Locator;
    readonly _loginButtonToLoginPage: Locator;
    readonly _errorMessageLabel: Locator;
    // readonly _appName: Locator;

    constructor(page: Page) {
        this._page = page;
        this._pageUrl = `${app.baseUrl}/auth/sign-in`;
        this._loginField = page.getByTestId('test-email');
        this._passwordField = page.getByTestId('test-password');
        this._loginButton = page.getByTestId('test-login-btn');
        this._loginButtonToLoginPage = page.getByTestId('test-sign-in');
        // this._appName = page.getByText('ห้องสมุดไอเดีย'); 
    }

    async visit() {
        await this._page.goto(this._pageUrl);
    }

    async goToLoginPage() {
        await this._loginButtonToLoginPage.click();
    }

    async loginWith(credential: Credential) {
        await this._loginField.fill(credential.login);
        await this._passwordField.fill(credential.password);
        await this._loginButton.click();
    }

    // async shouldBeDisplayed() {
    //     await this._page.waitForURL(this._pageUrl);
    //     await expect(this._appName).toBeVisible();
    //     await expect(this._appName).toHaveText('ห้องสมุดไอเดีย');
    //     await expect(this._page.url()).toEqual(this._pageUrl);
    // }

    async shouldContainErrorMessage(error: string) {
        await expect(this._errorMessageLabel).toBeVisible();
        await expect(this._errorMessageLabel).toHaveText(error);
    }
}
