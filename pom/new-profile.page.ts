import { Locator, Page, expect } from "@playwright/test";
import { app } from "../fixtures/app";
import { User } from "../interfaces/user";

export class NewProfile {
    readonly _page: Page;
    readonly _pageUrl: string;
    readonly _profileImage: Locator;
    readonly _displayName: Locator;
    readonly _dropDownProfile: Locator;

    constructor(page: Page, user: User) {
        this._page = page;
        this._pageUrl = `${app.baseUrl}/profile/${user.userid}`;
        this._profileImage = page.getByRole('img', { name: 'frame' });
        this._displayName = page.getByText(user.displayName, { exact: true });
        this._dropDownProfile = page.getByRole('link', { name: 'profile โปรไฟล์ของฉัน' });
    }

    async goToProfile() {
        await this._page.goto(this._pageUrl);
    }

    async goToProfileDashboard() {
        await this._profileImage.hover();
        await this._dropDownProfile.click();
    }

    async shouldBeDisplayedAfterLogin() {
        await expect(this._profileImage).toBeVisible();
        await expect(this._displayName).toBeVisible();
    }
}
