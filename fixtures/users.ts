import { User } from "../interfaces/user";

export const validUsers: User[] = [
  {
    displayName: "insKru",
    userid: 47,
    credential: {
      login: "inskru.ad@gmail.com",
      password: "insAdmin01",
    },
  },
  {
    displayName: "Nattapol Srirakullatad alias",
    userid: 174463,
    credential: {
      login: "nattapol@odds.team",
      password: "Abcdef123456",
    },
  },
];
